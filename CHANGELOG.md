# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## Added

- Add new criteria to list a server in the server browser.

## Changed

- Update the issue template to request new servers in the browser list.

## Fixed

## [0.1.0] - 2022-09-04

### Added

- First experimental backend

[unreleased]: https://github.com/veloren/Airshipper/compare/v0.1.0...master
