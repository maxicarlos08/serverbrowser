<!--
SPDX-FileCopyrightText: 2022 Marcel Märtens
SPDX-FileCopyrightText: 2022 Ben Wallis <https://gitlab.com/xvar>
SPDX-FileCopyrightText: 2023 - 2024 Javier Pérez

SPDX-License-Identifier: GPL-3.0-or-later
-->

# Server browser for Veloren

[![pipeline status](https://gitlab.com/veloren/serverbrowser/badges/master/pipeline.svg)](https://gitlab.com/veloren/veloren/commits/master)

Airshipper, the official game launcher for Veloren, is capable of displaying a
list of game servers to promote discoverability and ease of use.
This repository handles such list of servers.

## Inclusion of new servers to the list

If you want to request adding your server to the server browser,
you will need to raise a new issue using the `New Server` issue template.
You can create it here:

<https://gitlab.com/veloren/serverbrowser/-/issues/new?issuable_template=New%20Server>

### Requirements

Your server must meet the following requirements to be listed in the server
browser and remain there.

#### Technical

- The server must use the official authentication server.
- The server must use and keep the game version to the weekly release channel.
- The server must not have emojis in its name or description.
- The server must not have a name longer than 35 characters.

#### Finance

- It is acceptable to fund the server costs on platforms such as Open Collective.
- Donation methods associated to cryptocurrencies and non-fungible tokens (NFTs)
are frowned upon and will get your server listing application rejected.

#### Marketing

- The server cannot advertise itself in such a way which implies the core
development team or the official Veloren project endorse it.

#### Social

- The server must enforce rules of conduct. This list of rules, specific to
your server, must enforce some basic decency such as no bigotry.

#### Legal

- The server must comply with the terms and conditions of the licenses under
which the game is provided.

## Notes

- There is no guarantee that your request to add a server will be accepted.
- If you are unsure whether or not your server financing methods are admissible,
feel free to discuss this topic with us.
- The official authentication server is available at <https://auth.veloren.net>.
- The server must reply to ICMP echo requests from the Internet to display
its status correctly in the server browser.
- The server can be easily deployed and kept up-to-date with a `docker-compose.yml` file we provide. [Watchtower](https://containrrr.dev/watchtower/) is included.
The file is available here:
<https://gitlab.com/veloren/veloren/-/blob/master/server-cli/docker-compose.yml>

## FAQ

### Why isn't the server registration process automatic?

This is planned for a future release of the server browser. Manual registration
is only required until we implement the features needed to facilitate automatic
server registration.

### Why can't I use emojis in my server name or description?

Airshipper does not currently support rendering emoji characters. This
feature might be implemented in the future.

### What does the marketing rule about endorsement mean?

It means the server cannot be advertised in a way that people might think it is
an _official_ server. For instance, statements such as this are not acceptable:
> This is the official Veloren server in _&lt;country&gt;_!

It is possible to advertise your server as official only if you have gotten
endorsement from the core development team of the official Veloren project
to do so.
